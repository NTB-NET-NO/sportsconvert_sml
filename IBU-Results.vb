Imports System.IO
Imports System.xml
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Configuration.ConfigurationSettings
Imports ntb_FuncLib.LogFile

Public Class IBU_Results

    Protected Class athlete

        'General
        Public rank As String
        Public bib As String
        Public name As String
        Public nat_code As String
        Public nation As String

        'Biathlon specifics
        Public start As String
        Public time As String
        Public diff As String
        Public shoot_1 As String
        Public shoot_2 As String
        Public shoot_3 As String
        Public shoot_4 As String
        Public shoot_total As String

        Public points_total As String

        Protected logFolder As String = MainConvertModule.ProjectFolder(AppSettings("LogFolder"))

        'Constructors
        'Biathlon
        Public Sub New(ByVal rank_in As String, ByVal bib_in As String, ByVal name_in As String, ByVal natcode_in As String, ByVal time_in As String, ByVal diff_in As String, _
                       ByVal shoot_1_in As String, ByVal shoot_2_in As String, ByVal shoot_3_in As String, ByVal shoot_4_in As String, ByVal shoot_total_in As String)



            rank = rank_in
            bib = bib_in
            name = name_in
            nat_code = natcode_in

            nation = MainConvertModule.nation_map(nat_code)
            If nation = "" Then nation = natcode_in

            time = time_in
            diff = diff_in

            shoot_1 = shoot_1_in
            shoot_2 = shoot_2_in
            shoot_3 = shoot_3_in
            shoot_4 = shoot_4_in
            shoot_total = shoot_total_in

            name = ConvertTools.RotateAndFixName(name)

            'Character map substitutions
            Dim subst As String() = character_map(nat_code)
            If Not subst Is Nothing Then
                Dim i As Integer
                For i = 0 To subst.GetLength(0) - 1
                    name = name.Replace(subst(i), subst(i + 1))
                    i += 1
                Next
            End If

        End Sub

    End Class
    Protected athlete_list As ArrayList
    Protected Shared character_map As New Hashtable

    'Object constructor creates object and loads mapping lists
    Public Sub New()

        athlete_list = New ArrayList

        'Load character map
        Dim charmapfile As String = MainConvertModule.ProjectFolder(AppSettings("CharacterMapFile"))
        Dim map As New XmlDocument

        Try
            map.Load(charmapfile)

            Dim nodes As XmlNodeList
            Dim nd1, nd2 As XmlNode
            nodes = map.SelectNodes("/character-maps/nation")

            'Fill hash
            For Each nd1 In nodes
                If nd1.ChildNodes().Count > 0 Then

                    Dim c As Integer = 0
                    Dim charset((nd1.ChildNodes().Count * 2) - 1) As String
                    For Each nd2 In nd1.ChildNodes()
                        charset(c) = nd2.Attributes("value").Value
                        charset(c + 1) = nd2.Attributes("replace").Value
                        c += 2
                    Next

                    character_map(nd1.Attributes("code").Value) = charset
                End If
            Next

            WriteLog(New String(MainConvertModule.ProjectFolder(AppSettings("LogFolder"))), "IBU - Character converison map loaded (" & charmapfile & "): " & CStr(character_map.Count) & " conversion sets.")
        Catch ex As Exception
            'Error loading nation map
            WriteErr(New String(MainConvertModule.ProjectFolder(AppSettings("LogFolder"))), "Error loading charachter conversion map (" & charmapfile & ") for IBU", ex)
        End Try

    End Sub

    Protected Function ParseDataBI(ByVal filename As String) As Integer

        Dim input As StreamReader = New StreamReader(filename, Encoding.UTF8)
        Dim content As String = input.ReadToEnd()
        input.Close()

        athlete_list.Clear()

        'Remove tags
        Dim tags As Regex = New Regex("<[^>]+>", RegexOptions.Compiled) 'Drop all tags
        Dim tabs As Regex = New Regex("\t+", RegexOptions.Compiled) 'Drop all tags
        content = content.Replace("</td>", "|")
        content = tags.Replace(content, "")
        content = tabs.Replace(content, "")

        'Split off header
        Dim topsplit As String = "POS|BIB"
        content = content.Substring(content.IndexOf(topsplit) + topsplit.Length)

        'Find results
        Dim res As Regex = New Regex("^=?(\d+|DNS|DNF)\|(\d+)\|(&nbsp;|[yr])\|(.+?)\|(\w{3})\|(\d*)\+?(\d*)\+?(\d*)\+?(\d*)\|(\d+)?\|\s*([\d\.:]+)?\|\s*\+?([\d\.:]+)?\|", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)

        'Dim first As TimeSpan
        Dim m As Match = res.Match(content)
        While m.Success

            Dim rank As String = m.Groups(1).Value
            Dim bib As String = m.Groups(2).Value
            Dim name As String = m.Groups(4).Value
            Dim nation As String = m.Groups(5).Value
            Dim shoot_1 As String = m.Groups(6).Value.Trim()
            Dim shoot_2 As String = m.Groups(7).Value.Trim()
            Dim shoot_3 As String = m.Groups(8).Value.Trim()
            Dim shoot_4 As String = m.Groups(9).Value.Trim()
            Dim shoot_total As String = m.Groups(10).Value

            Dim time, diff As TimeSpan
            Dim strTime, strDiff As String

            If (m.Groups(11).Value <> "" And m.Groups(12).Value <> "") Then
                Dim pos As Int32 = m.Groups(11).Value.LastIndexOf(".")
                pos = m.Groups(11).Value.Length - pos

                If (pos > 2) Then
                    time = GetTimeSpan(m.Groups(11).Value.Replace(":", "."), 10)
                Else
                    time = GetTimeSpan(m.Groups(11).Value.Replace(":", "."))
                End If
                diff = GetTimeSpan(m.Groups(12).Value.Replace(":", "."))

                'Format times
                If (pos > 2) Then
                    strTime = ConvertTools.FormatTime(time, 10, 2)
                Else
                    strTime = ConvertTools.FormatTime(time)
                End If
                strDiff = ConvertTools.FormatTime(diff)
            End If

            Dim athlete As New athlete(rank, bib, name, nation, strTime, strDiff, shoot_1, shoot_2, shoot_3, shoot_4, shoot_total)
            athlete_list.Add(athlete)

            m = m.NextMatch()
        End While

        Return athlete_list.Count
    End Function

    Protected Function ParseDataBIRelay(ByVal filename As String) As Integer

        Dim input As StreamReader = New StreamReader(filename, Encoding.UTF8)
        Dim content As String = input.ReadToEnd()
        input.Close()

        athlete_list.Clear()

        'Remove tags
        Dim tags As Regex = New Regex("<[^>]+>", RegexOptions.Compiled) 'Drop all tags
        Dim tabs As Regex = New Regex("\t+", RegexOptions.Compiled) 'Drop all tags
        content = content.Replace("</td>", "|")
        content = tags.Replace(content, "")
        content = tabs.Replace(content, "")

        'Split off header
        Dim topsplit As String = "POS|BIB"
        content = content.Substring(content.IndexOf(topsplit) + topsplit.Length)

        'Find results
        Dim res As Regex = New Regex("^=?(\d+|DNS|DNF)\|(\d+)\|&nbsp;\|(.+?)\|(\w{3})\|(\d*)\+?(\d*)(&nbsp;&nbsp;)?(\d*)\+?(\d*)\|\|\s*([\d\.:]+)?\|\s*\+?([\d\.:]+)?\|", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)

        'Dim first As TimeSpan
        Dim m As Match = res.Match(content)
        While m.Success

            Dim rank As String = m.Groups(1).Value
            Dim bib As String = m.Groups(2).Value
            Dim name As String = m.Groups(3).Value.Trim()
            Dim nation As String = m.Groups(4).Value
            Dim shoot_1 As String = m.Groups(5).Value.Trim()
            Dim shoot_2 As String = m.Groups(6).Value.Trim()
            Dim shoot_3 As String = m.Groups(8).Value.Trim()
            Dim shoot_4 As String = m.Groups(9).Value.Trim()
            Dim shoot_total As String

            Dim time, diff As TimeSpan
            Dim strTime, strDiff As String

            If (m.Groups(10).Value <> "" And m.Groups(11).Value <> "") Then
                time = GetTimeSpan(m.Groups(10).Value.Replace(":", "."), 10)
                diff = GetTimeSpan(m.Groups(11).Value.Replace(":", "."))

                'Format times
                strTime = ConvertTools.FormatTime(time, 10, 2)
                strDiff = ConvertTools.FormatTime(diff)
            End If

            Dim athlete As New athlete(rank, bib, name, nation, strTime, strDiff, shoot_1, shoot_2, shoot_3, shoot_4, shoot_total)
            athlete_list.Add(athlete)

            m = m.NextMatch()
        End While

        Return athlete_list.Count
    End Function

    'Returns a formated resultlist, for individual starts
    Public Function BI_General(ByVal filename As String, ByVal format As MainConvertModule.FormatType, ByRef body As String) As String

        Dim c As Integer = ParseDataBI(filename)
        Dim ret As String = ""

        'Fill the athlete list
        If format = MainConvertModule.FormatType.Relay Then
            c = ParseDataBIRelay(filename)
        Else
            c = ParseDataBI(filename)
        End If

        Dim i As Integer = 1
        Dim at As athlete
        For Each at In athlete_list

            'Add paragraps
            If i = 1 Then
                ret &= "<p class=""txt"">"
            ElseIf i Mod 10 = 1 Then
                ret &= "</p>" & vbCrLf & "<p class=""txt-ind"">"
            End If

            'Fill in results
            Select Case format
                Case MainConvertModule.FormatType.Relay
                    ret &= at.rank & ") " & at.name & ", " & at.nation & ", " & at.time & " (" & _
                    (Convert.ToInt32(at.shoot_1) + Convert.ToInt32(at.shoot_3)) & "/" & _
                    (Convert.ToInt32(at.shoot_2) + Convert.ToInt32(at.shoot_4)) & "), "
                Case MainConvertModule.FormatType.IndividualStart
                    ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & " (" & at.shoot_total & "), "
                Case Else
                    If i = 1 Then
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & " (" & at.shoot_total & "), "
                    ElseIf i = 2 Then
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.diff & " min bak (" & at.shoot_total & "), "
                    Else
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.diff & " (" & at.shoot_total & "), "
                    End If
            End Select

            'Update counter
            i += 1
        Next

        'Finish off and return
        ret &= "</p>"
        body = ret

        Return c
    End Function

End Class
