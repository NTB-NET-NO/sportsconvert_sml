Imports System.IO
Imports System.xml
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Configuration.ConfigurationSettings
Imports SportsConvert.MainConvertModule

Imports ntb_FuncLib.LogFile

Public Class FIS_Live

    Protected Class athlete

        'General
        Public rank As String
        Public bib As String
        Public name As String
        Public nat_code As String
        Public nation As String

        'Cross country specifics
        Public start As String
        Public time As String
        Public diff As String
        Public status As String

        'Ski jumping specifics
        Public distance_1 As String
        Public distance_2 As String
        Public points_1 As String
        Public points_2 As String
        Public rank_1 As String
        Public rank_2 As String

        'Alpine specifics
        Public time2 As String
        Public total As String


        Public points_total As String

        'Constructors

        'Cross country
        ' This might have to be changed a bit if we want to do relay here as well... 
        Public Sub New(ByVal rank_in As String, ByVal bib_in As String, ByVal name_in As String, ByVal natcode_in As String, ByVal start_in As String, ByVal time_in As String, ByVal diff_in As String, ByVal status_in As String)
            rank = rank_in
            bib = bib_in
            name = name_in
            nat_code = natcode_in

            nation = MainConvertModule.nation_map(nat_code)
            If nation = "" Then nation = natcode_in

            'This is cross country
            start = start_in
            time = time_in
            diff = diff_in
            status = status_in

            name = ConvertTools.RotateAndFixName(name)

            'Character map substitutions
            Dim subst As String() = character_map(nat_code)
            If Not subst Is Nothing Then
                Dim i As Integer
                For i = 0 To subst.GetLength(0) - 1
                    name = name.Replace(subst(i), subst(i + 1))
                    i += 1
                Next
            End If

        End Sub
        'Alpine
        Public Sub New(ByVal rank_in As String, ByVal bib_in As String, ByVal name_in As String, ByVal natcode_in As String, ByVal time_in As String, ByVal time2_in As String, ByVal total_in As String)
            rank = rank_in
            bib = bib_in
            name = name_in
            nat_code = natcode_in
            'This is Alpine
            time = time_in
            time2 = time2_in
            total = total_in


            nation = MainConvertModule.nation_map(nat_code)
            If nation = "" Then nation = natcode_in



            'This is cross country
            ' start = start_in
            'time = time_in
            ' diff = diff_in
            'status = status_in

            name = ConvertTools.RotateAndFixName(name)

            'Character map substitutions
            Dim subst As String() = character_map(nat_code)
            If Not subst Is Nothing Then
                Dim i As Integer
                For i = 0 To subst.GetLength(0) - 1
                    name = name.Replace(subst(i), subst(i + 1))
                    i += 1
                Next
            End If

        End Sub
        'Ski jumping
        Public Sub New(ByVal rank_in As String, ByVal bib_in As String, ByVal name_in As String, ByVal natcode_in As String, _
                       ByVal distance_1_in As String, ByVal distance_2_in As String, ByVal points_1_in As String, ByVal points_2_in As String, _
                       ByVal rank_1_in As String, ByVal rank_2_in As String, ByVal points_total_in As String)
            rank = rank_in
            bib = bib_in
            name = name_in
            nat_code = natcode_in

            nation = MainConvertModule.nation_map(nat_code)
            If nation = "" Then nation = natcode_in

            name = ConvertTools.RotateAndFixName(name, ConvertTools.NameFormat.UCASE)

            'This is ski jumping
            distance_1 = distance_1_in
            distance_2 = distance_2_in
            points_1 = points_1_in
            points_2 = points_2_in
            rank_1 = rank_1_in
            rank_2 = rank_2_in

            points_total = points_total_in

            distance_1 = distance_1.Replace(".", ",").Replace("m", "").Replace(",0", "")
            distance_2 = distance_2.Replace(".", ",").Replace("m", "").Replace(",0", "")

            points_1 = points_1.Replace(".", ",")
            points_2 = points_2.Replace(".", ",")
            points_total = points_total.Replace(".", ",")

            'Character map substitutions
            Dim subst As String() = character_map(nat_code)
            If Not subst Is Nothing Then
                Dim i As Integer
                For i = 0 To subst.GetLength(0) - 1
                    name = name.Replace(subst(i), subst(i + 1))
                    i += 1
                Next
            End If

        End Sub

    End Class

    Protected athlete_list As ArrayList

    Protected Shared character_map As New Hashtable
    'Object constructor creates object and loads mapping lists
    Public Sub New()

        athlete_list = New ArrayList

        'Load character map
        Dim charmapfile As String = MainConvertModule.ProjectFolder(AppSettings("CharacterMapFile"))
        Dim map As New XmlDocument

        Try
            map.Load(charmapfile)

            Dim nodes As XmlNodeList
            Dim nd1, nd2 As XmlNode
            nodes = map.SelectNodes("/character-maps/nation")

            'Fill hash
            For Each nd1 In nodes
                If nd1.ChildNodes().Count > 0 Then

                    Dim c As Integer = 0
                    Dim charset((nd1.ChildNodes().Count * 2) - 1) As String
                    For Each nd2 In nd1.ChildNodes()
                        charset(c) = nd2.Attributes("value").Value
                        charset(c + 1) = nd2.Attributes("replace").Value
                        c += 2
                    Next

                    character_map(nd1.Attributes("code").Value) = charset
                End If
            Next

            WriteLog(New String(ProjectFolder(AppSettings("LogFolder"))), "FIS - Character conversion map loaded (" & charmapfile & "): " & character_map.Count & " conversion sets.")
        Catch ex As Exception
            'Error loading nation map
            WriteErr(New String(ProjectFolder(AppSettings("LogFolder"))), "Error loading charachter conversion map (" & charmapfile & ") for FIS", ex)
        End Try

    End Sub
    'Takes a cross country HTML list and fills the athlete list
    Protected Function ParseDataCC(ByVal filename As String) As Integer
        Dim logFolder As String = MainConvertModule.ProjectFolder(AppSettings("LogFolder"))
        Dim input As StreamReader = New StreamReader(filename, Encoding.GetEncoding("ISO-8859-1"))
        Dim content As String = input.ReadToEnd()
        input.Close()

        athlete_list.Clear()

        'Remove tags
        Dim tags As Regex = New Regex("<[^>]+>", RegexOptions.Compiled) 'Drop all tags
        content = tags.Replace(content, "")

        'Split off header
        Dim topsplit As String = "qualified for final A / B"
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "FIS Points"
        End If
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "Nation" & vbLf & "Result"
        End If
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "Nation" & vbCrLf & "Result"
        End If
        content = content.Substring(content.IndexOf(topsplit) + topsplit.Length)

        'Try primary data scource parse

        ' The line below grabs the relay results, but only the country. 
        ' &nbsp;(\d+)\s+&nbsp;\s+([\w\- \/\.]+)&nbsp;\s+&nbsp;\s+(\w{3})\s+([\d:\.]+)\s+&nbsp;\s+&nbsp;\s+
        Dim res As Regex
        If MainConvertModule.FormatType.Relay Then
            res = New Regex("&nbsp;(\d+)\s+&nbsp;\s+([\w\- \/\.]+)&nbsp;\s+&nbsp;\s+(\w{3})\s+([\d:\.]+)\s+&nbsp;\s+&nbsp;\s+", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        Else
            res = New Regex("&nbsp;(\d+|\w{3})&nbsp;\s+(\d+)&nbsp;\s+&nbsp;([\w\- \/\.&;]+?)(&nbsp;)?\s+&nbsp;(\w{3})\s+&nbsp;\s+([*\d :]+)&nbsp;\s+([\d:\.]*)&nbsp;\s+([\d:\.\+]*)&nbsp;\s+&nbsp;(\w*)\s*", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        End If
        'Find results 1
        Dim sel As Integer = 1
        ' Dim res As Regex = New Regex("&nbsp;(\d+|\w{3})&nbsp;\s+(\d+)&nbsp;\s+&nbsp;([\w\- \/\.&;]+?)(&nbsp;)?\s+&nbsp;(\w{3})\s+&nbsp;\s+([*\d :]+)&nbsp;\s+([\d:\.]*)&nbsp;\s+([\d:\.\+]*)&nbsp;\s+&nbsp;(\w*)\s*", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        Dim m As Match = res.Match(content)

        'Find results 2
        If Not m.Success Then
            sel = 2
            res = New Regex("(\d+|\w{3})\s+(\d+)\s+([\w\- \/\.&;]+?)\s+(&nbsp;)?(\w{3})\s+([*\d :]+)\s+([\d:\.]*)\s+([\d:\.\+]*)", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
            m = res.Match(content)
        End If

        While m.Success
            Dim rank As String
            Dim bib As String
            Dim name As String
            Dim nation As String
            Dim start As String
            Dim time As String
            Dim diff As String
            Dim status As String



            'Sel 1
            If sel = 1 Then
                If MainConvertModule.FormatType.Relay Then
                    rank = m.Groups(1).Value
                    bib = Nothing
                    name = m.Groups(2).Value
                    nation = m.Groups(3).Value
                    start = Nothing ' m.Groups(6).Value
                    time = m.Groups(4).Value
                    diff = m.Groups(4).Value
                    status = Nothing ' status = m.Groups(9).Value
                Else

                    rank = m.Groups(1).Value
                    bib = m.Groups(2).Value
                    name = m.Groups(3).Value
                    nation = m.Groups(5).Value
                    start = m.Groups(6).Value
                    time = m.Groups(7).Value
                    diff = m.Groups(8).Value
                    status = m.Groups(9).Value
                End If
            ElseIf sel = 2 Then
                    'Sel 2
                    rank = m.Groups(1).Value
                    bib = m.Groups(2).Value
                    name = m.Groups(3).Value
                    nation = m.Groups(5).Value
                    start = m.Groups(6).Value
                    time = m.Groups(7).Value
                    diff = m.Groups(8).Value
                    status = m.Groups(9).Value
                End If


                'Format the times
                Dim span As TimeSpan = ConvertTools.GetTimeSpan(time)
                time = ConvertTools.FormatTime(span)

                span = ConvertTools.GetTimeSpan(diff)
                diff = ConvertTools.FormatTime(span)

                Dim athlete As New athlete(rank, bib, name, nation, start, time, diff, status)
                athlete_list.Add(athlete)

                m = m.NextMatch()
        End While

        'If none found try secondary
        If athlete_list.Count = 0 Then

            Dim first As TimeSpan
            WriteLog(logFolder, "We are trying secondary because we haven't found anything")
            res = New Regex("&nbsp;(\d+)\s+&nbsp;(\d+)?\s+&nbsp;(\d+)\s+([\w\- \/\.]+)&nbsp;\s+(\d+)?&nbsp;\s+(\w{3})&nbsp;\s+&nbsp;([\d:\.]+)?(\s+&nbsp;)??([\d\.]+)?", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
            m = res.Match(content)
            While m.Success

                Dim rank As String = m.Groups(1).Value
                Dim bib As String = m.Groups(2).Value
                Dim name As String = m.Groups(4).Value
                Dim nation As String = m.Groups(6).Value
                Dim start As String = ""
                Dim time As String = m.Groups(7).Value
                Dim diff As String = ""
                Dim status As String = ""

                'Format the times
                If time <> "" Then
                    Dim t, d As TimeSpan
                    If rank = 1 Then
                        first = GetTimeSpan(time)
                        t = first
                        d = New TimeSpan(0)
                    Else
                        t = GetTimeSpan(time)
                        d = t.Subtract(first)
                    End If

                    time = ConvertTools.FormatTime(t)
                    diff = ConvertTools.FormatTime(d)
                End If

                Dim athlete As New athlete(rank, bib, name, nation, start, time, diff, status)
                athlete_list.Add(athlete)

                m = m.NextMatch()
            End While

        End If

        Return athlete_list.Count
    End Function

    Protected Function ParseDataAL(ByVal filename As String, ByVal format As MainConvertModule.FormatType) As Integer
        ' -----------------------------------------------------------------------------
        ' ParseDataAL
        '
        ' Description:
        '   Takes a alpine HTML list and fills the athlete list
        '   
        ' Parameters:
        '
        ' Returns:
        '
        ' Notes :
        '   Rev 2008-01-14, Trond Hus�: Alpine can consist of two runs, and there are therefor two folders for this.
        '                   in this revision I have changed the code so that it checks which folder the result has been saved in.
        '                   This because the sel-check does not work like it was supposed to. 
        ' -----------------------------------------------------------------------------

        Dim input As StreamReader = New StreamReader(filename, Encoding.GetEncoding("ISO-8859-1"))
        ' Dim logFolder As String = My.Settings.LogFolder ' Dim logFolder As String = MainConvertModule.ProjectFolder(AppSettings("LogFolder"))
        Dim logFolder As String = MainConvertModule.ProjectFolder(AppSettings("LogFolder"))
        Dim content As String = input.ReadToEnd()
        input.Close()

        WriteLog(logFolder, "Da er vi i ParseDataAL")
        athlete_list.Clear()

        'Remove tags
        Dim tags As Regex = New Regex("<[^>]+>", RegexOptions.Compiled) 'Drop all tags
        content = tags.Replace(content, "")

        ' Here we find out if we are working with results from Slalom (SL) or giant slalom (SSL)
        Dim dicipline As String = Nothing
        If content.IndexOf("Giant Slalom") <> -1 Then
            dicipline = "SSL"
        ElseIf content.IndexOf("Slalom") <> -1 Then
            dicipline = "SL"
        End If

        'Split off header
        Dim topsplit As String = "qualified for final A / B"
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "FIS Points"
        End If
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "Nation" & vbLf & "Result"
        End If
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "Nation" & vbCrLf & "Result"
        End If
        content = content.Substring(content.IndexOf(topsplit) + topsplit.Length)


        ' This part here will be changed so that we check if the folder used is either alpint-en or alpint-to
        ' The first means that we are to parse downhill or Super-G.
        ' The second means that we are to parse Super combination, combination, slalom and giant slalom. 

        ' We check if we have the job from alpint-en
        Dim res As Regex = Nothing
        Dim m As Match
        If format = FormatType.OneRound Then
            ' Run regexp to get the results for one run alpine event. 
            WriteLog(logFolder, "We are in one round")
            res = New Regex("&nbsp;(\d+)\s+&nbsp;(\d+)?\s+&nbsp;(\d+)\s+([\w\- \/\.]+)&nbsp;\s+(\d+)?&nbsp;\s+(\w{3})&nbsp;\s+&nbsp;([\d:\.]+)?(\s+&nbsp;)??([\d\.]+)?\s+\s+", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        ElseIf format = FormatType.TwoRounds Then
            ' Run regexp to get the results for alpine events that has two runs. 
            WriteLog(logFolder, "We are in two rounds")
            res = New Regex("&nbsp;(\d+)\s+&nbsp;(\d+)?\s+&nbsp;(\d+)\s+([\w\- \/\.]+)&nbsp;\s+(\d+)?&nbsp;\s+(\w{3})&nbsp;\s+&nbsp;([\d:\.]+)?(\s+&nbsp;)??([\d\.]+)\s+&nbsp;([\d:\.]+)?(\s+&nbsp;)??([\d\.]+)?\s+&nbsp;([\d:\.]+)?(\s+&nbsp;)??([\d\.]+)?", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
            ' res = New Regex("&nbsp;(\d+)\s+&nbsp;(\d+)?\s+&nbsp;(\d+)\s+([\w\- \/\.]+)&nbsp;\s+(\d+)?&nbsp;\s+(\w{3})&nbsp;\s+&nbsp;([\d:\.]+)?(\s+&nbsp;)??([\d\.]+)\s+&nbsp;([\d:\.]+)?(\s+&nbsp;)??([\d\.]+)?", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
            ' &nbsp;(\d+)\s+&nbsp;(\d+)?\s+&nbsp;(\d+)\s+([\w\- \/\.]+)&nbsp;\s+(\d+)?&nbsp;\s+(\w{3})&nbsp;\s+&nbsp;([\d:\.]+)?(\s+&nbsp;)??([\d\.]+)\s+&nbsp;([\d:\.]+)?(\s+&nbsp;)??([\d\.]+)?\s+&nbsp;([\d:\.]+)?(\s+&nbsp;)??([\d\.]+)?
            ' Linjen over fungerer p� en ssl...

        End If

        ' run the regex lines... 
        m = res.Match(content)

        ' When parsing european cup we don't get any data, so we have to run this instead:
        Dim europeancup As Boolean = False

        If Not m.Success Then
            ' This is most likely because we just got a result from the european cup... 
            ' This is just a quick and dirty workaround. We should check if we find european cup in the document... 
            If format = FormatType.OneRound Then
                res = New Regex("&nbsp;(\d+)\s+&nbsp;(\d+)?\s+&nbsp;(\d+)\s+([\w\- \/\.]+)&nbsp;\s+(\d+)?&nbsp;\s+(\w{3})&nbsp;\s+&nbsp;([\d:\,]+)\s+&nbsp;", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
            ElseIf format = FormatType.TwoRounds Then
                res = New Regex("&nbsp;(\d+)\s+&nbsp;(\d+)?\s+&nbsp;(\d+)\s+([\w\- \/\.]+)&nbsp;\s+(\d+)?&nbsp;\s+(\w{3})&nbsp;\s+&nbsp;([\d:\,]+)\s+&nbsp;([\d:\,]+)\s+&nbsp;([\d:\,]+)\s+", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
            End If

            m = res.Match(content)
            europeancup = True
        End If


        While m.Success
            Dim rank As String = Nothing
            Dim bib As String = Nothing
            Dim name As String = Nothing
            Dim nation As String = Nothing
            Dim time2 As String = Nothing
            Dim time As String = Nothing
            Dim total As String = Nothing
            Dim start As String = Nothing
            Dim diff As String = Nothing
            Dim status As String = Nothing


            ' This is for debug purposes only. 
            Dim a As Integer
            For a = 1 To m.Groups.Count
                WriteLog(logFolder, "Dette er innholdet i m.Groups(" & a & "): " & m.Groups(a).Value.ToString)
            Next

            If format = FormatType.OneRound Then
                rank = m.Groups(1).Value
                bib = m.Groups(2).Value
                name = m.Groups(4).Value
                nation = m.Groups(6).Value
                time = m.Groups(7).Value
                time2 = m.Groups(8).Value
                total = m.Groups(9).Value
            ElseIf format = FormatType.TwoRounds Then
                ' Her har vi (selvf�lgelig) st�tt p� et problem. 
                ' Hvis det handler om slal�m st�r totaltiden i array 10, men hvis det er storslal�m er det array 13...
                ' S� vi m� finne ut hva vi har.

                rank = m.Groups(1).Value
                bib = m.Groups(2).Value
                name = m.Groups(4).Value
                nation = m.Groups(6).Value
                time = m.Groups(7).Value

                If Len(m.Groups(9).Value) = 1 Then
                    time = m.Groups(7).Value & m.Groups(9).Value
                    time2 = m.Groups(10).Value
                    total = m.Groups(13).Value

                Else
                    time2 = m.Groups(9).Value
                    total = m.Groups(10).Value
                End If



                If europeancup = True Then
                    ' Sometimes there are results from the European cup. For some reason this file is generated a bit different
                    ' So then value of array 8 is empty, so we add this instead. And total is then array 9.
                    time = m.Groups(7).Value
                    time2 = m.Groups(8).Value
                    total = m.Groups(9).Value


                End If

            End If





            'F�rstel�pstid
            ' Dim span As TimeSpan = ConvertTools.GetTimeSpan(time, 1)
            Dim span As TimeSpan = ConvertTools.GetTimeSpan(time, 1)
            'F�rstel�pstid
            time = ConvertTools.FormatTimeAl(span)

            'Andrel�pstid
            If format = FormatType.TwoRounds Then

                If time2 <> Nothing Then
                    WriteLog(logFolder, "Vi er i two rounds og vi skal konvertere annen omgang")
                    span = ConvertTools.GetTimeSpan(time2, 1)
                    time2 = ConvertTools.FormatTimeAl(span)
                End If
            End If

            If total.ToString <> Nothing Then
                'Total tid
                span = ConvertTools.GetTimeSpan(total, 1)
                total = ConvertTools.FormatTimeAl(span)

            End If

            ' I cut from here. 



            'span = ConvertTools.GetTimeSpan(diff)
            'diff = ConvertTools.FormatTime(span)

            Dim athlete As New athlete(rank, bib, name, nation, time, time2, total)
            athlete_list.Add(athlete)

            m = m.NextMatch()
        End While

        'If none found try secondary
        If athlete_list.Count = 0 Then
            WriteLog(logFolder, "Trying secondary")

            Dim first As TimeSpan


            ' res = New Regex("&nbsp;(\d+)\s+&nbsp;(\d+)?\s+&nbsp;(\d+)\s+([\w\- \/\.]+)&nbsp;\s+(\d+)?&nbsp;\s+(\w{3})&nbsp;\s+&nbsp;([\d:\.]+)?(\s+&nbsp;)??([\d\.]+)? &nbsp;([\d:\.]+)?", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
            res = New Regex("&nbsp;(\d+)\s+&nbsp;(\d+)?\s+&nbsp;(\d+)\s+([\w\- \/\.]+)&nbsp;\s+(\d+)?&nbsp;\s+(\w{3})&nbsp;\s+&nbsp;([\d:\,]+)\s+&nbsp;([\d:\,]+)\s+&nbsp;([\d:\,]+)\s+", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
            m = res.Match(content)
            While m.Success

                Dim rank As String = m.Groups(1).Value
                Dim bib As String = m.Groups(2).Value
                Dim name As String = m.Groups(4).Value
                Dim nation As String = m.Groups(6).Value
                Dim start As String = ""
                Dim time As String = m.Groups(7).Value
                Dim diff As String = ""
                Dim status As String = ""
                Dim time2 As String = m.Groups(8).Value
                Dim total As String = m.Groups(9).Value

                ' Run a replace on time just in case it has a , (comma) in it
                time = Replace(time, ",", ".")
                time2 = Replace(time2.ToString, ",", ".")
                total = Replace(total.ToString, ",", ".")


                'Format the times
                If time <> "" Then
                    Dim t, d As TimeSpan
                    If rank = 1 Then
                        first = GetTimeSpan(time, 1)
                        t = first
                        d = New TimeSpan(0)
                    Else
                        t = GetTimeSpan(time, 1)
                        d = t.Subtract(first)
                    End If

                    time = ConvertTools.FormatTimeAl(t)
                    diff = ConvertTools.FormatTimeAl(d)
                End If

                Dim athlete As New athlete(rank, bib, name, nation, time, time2, total)
                athlete_list.Add(athlete)

                m = m.NextMatch()
            End While

        End If

        Return athlete_list.Count
    End Function
    'Takes a ski jumping HTML list and fills the athlete list
    Protected Function ParseDataSJ(ByVal filename As String) As Integer

        Dim input As StreamReader = New StreamReader(filename, Encoding.GetEncoding("ISO-8859-1"))
        Dim content As String = input.ReadToEnd()
        input.Close()

        athlete_list.Clear()

        'Remove tags
        Dim tags As Regex = New Regex("<[^i][^>]*>", RegexOptions.Compiled Or RegexOptions.IgnoreCase)  'Keep images
        content = tags.Replace(content, "")

        'Split off header
        Dim topsplit As String = "/leer.gif"" width=""1"" height=""1"">"
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "Points"
        End If
        content = content.Substring(content.IndexOf(topsplit) + topsplit.Length)


        'Find results from primary source 1
        Dim sel As Integer = 1
        Dim res As Regex = New Regex("\s+(\d+)(&nbsp;)+\s+(\d+)(&nbsp;)+\s+([\w\- \/\.&;]+?)\s+<IMG.+(\w{3})\.gif.+>\s+" & _
                           "([\d\.m]+)&nbsp;&nbsp;([\d\.m]+)&nbsp;&nbsp;\s+([\d\.]+)&nbsp;&nbsp;([\d\.]+)&nbsp;&nbsp;\s+" & _
                           "([\d]+)&nbsp;&nbsp;([\d]+)&nbsp;&nbsp;\s+([\d\.]+)&nbsp;", RegexOptions.IgnoreCase Or RegexOptions.Compiled)
        Dim m As Match = res.Match(content)

        'Find results from primary source 2
        If Not m.Success Then
            sel = 2
            res = New Regex("(\d+)\s+(&nbsp;)+\s+(\d+)(&nbsp;)+\s+([\w\- \/\.&;]+?)\s+<IMG.+(\w{3})\.gif.+>&nbsp;(\w{3})\s+([\d\.m]+) &nbsp;&nbsp;\s+([\d\.]+)&nbsp;&nbsp;", RegexOptions.IgnoreCase Or RegexOptions.Compiled)
            m = res.Match(content)
        End If

        While m.Success
            Dim rank As String
            Dim bib As String
            Dim name As String
            Dim nation As String
            Dim distance_1 As String
            Dim distance_2 As String
            Dim points_1 As String
            Dim points_2 As String
            Dim rank_1 As String
            Dim rank_2 As String
            Dim points_total As String

            If sel = 1 Then
                rank = m.Groups(1).Value
                bib = m.Groups(3).Value
                name = m.Groups(5).Value
                nation = m.Groups(6).Value
                distance_1 = m.Groups(7).Value
                distance_2 = m.Groups(8).Value
                points_1 = m.Groups(9).Value
                points_2 = m.Groups(10).Value
                rank_1 = m.Groups(11).Value
                rank_2 = m.Groups(12).Value
                points_total = m.Groups(13).Value
            ElseIf sel = 2 Then
                rank = m.Groups(1).Value
                bib = m.Groups(3).Value
                name = m.Groups(5).Value
                nation = m.Groups(6).Value
                distance_1 = m.Groups(8).Value
                distance_2 = ""
                points_1 = m.Groups(9).Value
                points_2 = ""
                rank_1 = m.Groups(1).Value
                rank_2 = ""
                points_total = m.Groups(9).Value
            End If

            Dim athlete As New athlete(rank, bib, name, nation, distance_1, distance_2, points_1, points_2, rank_1, rank_2, points_total)
            athlete_list.Add(athlete)

            m = m.NextMatch()
        End While

        'Try secondary source
        If athlete_list.Count = 0 Then

            'Find results
            res = New Regex("&nbsp;(\d+)\s+&nbsp;(\d+)\s+&nbsp;(\d+)\s+([\w\- \/\.]+)&nbsp;\s+(\d+)&nbsp;\s+(\w{3})&nbsp;\s+&nbsp;([\d\.]+)\s+&nbsp;([\d\.]+)?\s+&nbsp;([\d\.]+)", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)

            m = res.Match(content)
            While m.Success

                Dim rank As String = m.Groups(1).Value
                Dim bib As String = m.Groups(2).Value
                Dim name As String = m.Groups(4).Value
                Dim nation As String = m.Groups(6).Value
                Dim distance_1 As String = m.Groups(7).Value
                Dim distance_2 As String = m.Groups(8).Value
                Dim points_1 As String = ""
                Dim points_2 As String = ""
                Dim rank_1 As String = ""
                Dim rank_2 As String = ""

                Dim points_total As String = m.Groups(9).Value

                Dim athlete As New athlete(rank, bib, name, nation, distance_1, distance_2, points_1, points_2, rank_1, rank_2, points_total)
                athlete_list.Add(athlete)

                m = m.NextMatch()
            End While

        End If

        Return athlete_list.Count
    End Function
    Protected Function ParseDataCup(ByVal filename As String) As Integer
        Dim input As StreamReader = New StreamReader(filename, Encoding.GetEncoding("ISO-8859-1"))
        Dim logFolder As String = MainConvertModule.ProjectFolder(AppSettings("LogFolder"))
        Dim content As String = input.ReadToEnd()
        input.Close()

        athlete_list.Clear()

        'Remove tags
        Dim tags As Regex = New Regex("<[^i][^>]*>", RegexOptions.Compiled Or RegexOptions.IgnoreCase)  'Keep images
        content = tags.Replace(content, "")

        'Split off header
        Dim topsplit As String = "/leer.gif"" width=""1"" height=""1"">"
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "Points"
        End If
        content = content.Substring(content.IndexOf(topsplit) + topsplit.Length)

        'Find results from primary source 1
        Dim sel As Integer = 1
        Dim res As Regex = New Regex("&nbsp;\s+([\w\- \/\.]+)&nbsp;\s+(\w{3})&nbsp;\s+(\d+)\s+(\d+)\s+", _
            RegexOptions.IgnoreCase Or RegexOptions.Compiled)
        Dim m As Match = res.Match(content)


        While m.Success
            Dim rank As String = m.Groups(3).Value
            Dim bib As String = ""
            Dim name As String = m.Groups(1).Value
            Dim nation As String = m.Groups(2).Value
            Dim distance_1 As String = ""
            Dim distance_2 As String = ""
            Dim points_1 As String = ""
            Dim points_2 As String = ""
            Dim rank_1 As String = ""
            Dim rank_2 As String = ""
            Dim points_total As String = m.Groups(4).Value
            Dim a As Integer
            For a = 0 To m.Groups.Count
                WriteLog(logFolder, "Dette er verdien av m.groups(" & a & "): " & m.Groups(a).Value.ToString)
            Next


            Dim athlete As New athlete(rank, bib, name, nation, distance_1, distance_2, points_1, points_2, rank_1, rank_2, points_total)
            ' Dim athlete As New athlete(rank, "", name, nation, "", "", "", "", "", "", points_total)
            athlete_list.Add(athlete)

            m = m.NextMatch()
        End While



        Return athlete_list.Count
    End Function
    Public Function FIS_General(ByVal filename As String, ByVal format As MainConvertModule.FormatType, ByRef body As String) As Integer
        'Fill the athlete list
        Dim logFolder As String = MainConvertModule.ProjectFolder(AppSettings("LogFolder"))
        Dim c As Integer = ParseDataCup(filename)
        Dim ret As String = ""

        Dim i As Integer = 1
        Dim at As athlete
        For Each at In athlete_list

            'Add paragraps
            If i = 1 Then
                ' ret &= "<p class=""Brdtekst"">"
                ret &= "<p class=""txt"">"
            ElseIf i Mod 10 = 1 Then
                ' ret &= "</p>" & vbCrLf & "<p class=""BrdtekstInnrykk"">"
                ret &= "</p>" & vbCrLf & "<p class=""txt-ind"">"
            End If

            'Fill in results
            ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.points_total & ", "

            'Update counter
            i += 1
        Next

        'Finish off and return
        ret &= "</p>"
        body = ret

        Return c
    End Function
    'Returns a formated resultlist, for individual starts
    Public Function CC_General(ByVal filename As String, ByVal format As MainConvertModule.FormatType, ByRef body As String) As Integer

        'Fill the athlete list
        Dim c As Integer = ParseDataCC(filename)
        Dim ret As String = ""

        Dim i As Integer = 1
        Dim at As athlete
        For Each at In athlete_list

            'Add paragraps
            If i = 1 Then
                ' ret &= "<p class=""Brdtekst"">"
                ret &= "<p class=""txt"">"
            ElseIf i Mod 10 = 1 Then
                ' ret &= "</p>" & vbCrLf & "<p class=""BrdtekstInnrykk"">"
                ret &= "</p>" & vbCrLf & "<p class=""txt-ind"">"
            End If

            'Fill in results
            Select Case format
                Case MainConvertModule.FormatType.IndividualStart
                    ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                Case MainConvertModule.FormatType.Relay
                    ret &= at.rank & ") " & at.nation & " " & at.time & ", "
                Case Else
                    If i = 1 Then
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                    ElseIf i = 2 Then
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.diff & " min bak, "
                    Else
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.diff & ", "
                    End If
            End Select

            'Update counter
            i += 1
        Next

        'Finish off and return
        ret &= "</p>"
        body = ret

        Return c
    End Function

    Public Function AL_General(ByVal filename As String, ByVal format As MainConvertModule.FormatType, ByRef body As String) As Integer
        ' -----------------------------------------------------------------------------
        ' AL_General
        '
        ' Description:
        '   Returns a formated resultlist, for alpine
        '   
        ' Parameters:
        '
        ' Returns:
        '
        ' Notes :
        '       rev 2008.01.14, Trond Hus�: Added format in call to ParseDataAL so that we get the correct format type.
        '
        ' -----------------------------------------------------------------------------

        Dim logFolder As String = MainConvertModule.ProjectFolder(AppSettings("LogFolder"))
        'Fill the athlete list
        WriteLog(logFolder, "This is format: " & format.ToString)
        Dim c As Integer = ParseDataAL(filename, format)
        Dim ret As String = ""

        Dim i As Integer = 1
        Dim at As athlete
        For Each at In athlete_list

            'Add paragraps
            If i = 1 Then
                ' ret &= "<p class=""Brdtekst"">"
                ret &= "<p class=""txt"">"
            ElseIf i Mod 10 = 1 Then
                ' ret &= "</p>" & vbCrLf & "<p class=""BrdtekstInnrykk"">"
                ret &= "</p>" & vbCrLf & "<p class=""txt-ind"">"
            End If

            'Fill in results
            Select Case format
                Case MainConvertModule.FormatType.TwoRounds 'format = "TwoRounds"
                    WriteLog(logFolder, "Two Rounds")
                    at.time = Replace(at.time, "0.", "")
                    at.time2 = Replace(at.time2, "0.", "")

                    ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.total & " (" & at.time & "-" & at.time2 & "), "

                Case MainConvertModule.FormatType.OneRound
                    WriteLog(logFolder, "One Round")
                    ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                    'Case MainConvertModule.FormatType.KombineretNormal
                    '  ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "

                Case Else
                    ret &= " Denne falt gjennom til ELSE i fill result"



                    'Case Else
                    '   If i = 1 Then
                    '  ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                    ' ' ElseIf i = 2 Then
                    ''   ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & " min bak, "
                    'Else
                    '    ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                    'End If
            End Select

            'Update counter
            i += 1
        Next

        'Finish off and return
        ret &= "</p>"
        body = ret

        Return c
    End Function
    'Returns a formated resultlist, for ski jumping
    Public Function SJ_General(ByVal filename As String, ByVal format As MainConvertModule.FormatType, ByRef body As String) As Integer

        Dim c As Integer = ParseDataSJ(filename)
        Dim ret As String = ""

        Dim i As Integer = 1
        Dim at As athlete
        For Each at In athlete_list

            'Add paragraps
            If i = 1 Then
                ret &= "<p class=""txt"">"
                ' ret &= "<p class=""Brdtekst"">"
            ElseIf i Mod 10 = 1 Then
                ' ret &= "</p>" & vbCrLf & "<p class=""BrdtekstInnrykk"">"
                ret &= "</p>" & vbCrLf & "<p class=""txt-ind"">"
            End If

            ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.points_total & String.Format(" ({0}-{1}), ", at.distance_1, at.distance_2)


            'Update counter
            i += 1
        Next

        'Finish off and return
        ret &= "</p>"
        body = ret

        ' Since events can be stopped after one run, we will then have a resultlist that looks bad, so we
        ' replace -) with ) to make it look correct. 
        body = Replace(body, "-)", ")")
        Return c
    End Function

End Class
