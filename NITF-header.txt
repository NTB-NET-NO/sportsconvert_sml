<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>
<nitf version="-//IPTC//DTD NITF 3.2//EN" change.date="October 10, 2003" change.time="19:30" baselang="no-NO">
<head>
<title><!--tittel--></title>
<meta name="timestamp" content="2007.11.25 22:07:13" />
<meta name="subject" content="<!--stikkord-->" />
<meta name="foldername" content="Notabene\Deskmapper\NTB\Sportsdesken" />
<meta name="filename" content="" />
<meta name="NTBTjeneste" content="Nyhetstjenesten" />
<meta name="NTBMeldingsSign" content="sporten" />
<meta name="NTBPrioritet" content="5" />
<meta name="NTBMeldingsSign" content="<!--signatur-->" />
<meta name="NTBStikkord" content="<!--stikkord-->" />
<meta name="NTBDistribusjonsKode" content="ALL" />
<meta name="NTBBilderAntall" content="0" />
<meta name="NTBKanal" content="A" />
<meta name="NTBSendTilDirekte" content="False" />
<meta name="NTBSendTilDirektePluss" content="" />
<meta name="NTBMeldingsType" content="Sportsresultater" />
<meta name="NTBMeldingsSign" content="Sporten" />
<meta name="NTBFulltNavn" content="NTB Sport" />
<meta name="NTBEpostAdresse" content="sporten@ntb.no" />
<meta name="NTBBrukerSign" content="Sporten" />
<meta name="NTBSelskap" content="NTB" />
<meta name="NTBAvdeling" content="Sporten" />
<meta name="NTBOpprettetAv" content="Sporten" />
<meta name="NTBDistribusjon" content="Nyhetstjenesten(ALL)" />
<tobject tobject.type="Sport">
<tobject.property tobject.property.type="Tabeller og resultater" />
<tobject.subject tobject.subject.code="SPO" tobject.subject.refnum="15000000" tobject.subject.type="Sport" />
<tobject.subject tobject.subject.code="SPO" tobject.subject.refnum="15002000" tobject.subject.matter="<!--kategori-->" />
</tobject>
<docdata>
<ed-msg info="Husk � dobbeltsjekke navn og nasjonalitet! Aksenterte bokstaver og landskoder er fors�kt konvertert, men dette m� sjekkes." /> 
<doc.copyright year="2009" holder="NTB" /> 
</docdata>
<revision-history name="" />
</head>
<body>
<body.head>
<hedline>
<hl1><!--tittel--></hl1>
</hedline>
<distributor><org>NTB</org></distributor>
</body.head>
<body.content>
<!--body-->
</body.content>
<body.end>
<tagline>
</tagline>
</body.end>
</body>
</nitf>

