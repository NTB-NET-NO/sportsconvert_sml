Imports System.IO
Imports System.Text
Imports System.xml
Imports System.Configuration.ConfigurationSettings
Imports ntb_FuncLib.LogFile

Public Class MainConvertModule
    Inherits System.ComponentModel.Component

#Region " Component Designer generated code "

    Public Sub New(ByVal Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(Me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    Protected Friend WithEvents PollTimer As System.Timers.Timer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.PollTimer = New System.Timers.Timer
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PollTimer
        '
        Me.PollTimer.Interval = 5000
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    'Types of events
    Public Enum EventType
        CrossCountry
        SkiJump
        Biathlon
        Alpint
        Kombinert
        SpeedSkating
        FIS
        NM
    End Enum

    'Formats
    Public Enum FormatType
        IndividualStart
        CommonStart
        Relay
        OneRound
        TwoRounds
        KombineretNormal
        SpeedSkatingNormal
        FIS
    End Enum

    Protected Structure FolderJob

        'Folder to poll
        Public folder As String

        'Type og event, e.g. cross country, biathlon etc
        Public type As EventType

        'Format of resulting file
        Public format As FormatType

    End Structure

    Protected JobList As New ArrayList

    Public Shared nation_map As New Hashtable
    Protected FISconvert As FIS_Live
    Protected IBUconvert As IBU_Results
    Protected SCGconvert As SCG_Results
    Protected NMconvert As FIS_NM

    Protected fileWait As Integer = 2
    Protected timerInterval As Integer = 30000 ' 30000 til produskjonsversjonen
    Protected notabeneError As Boolean = False

    Protected notabeneOutput As String = ProjectFolder(AppSettings("OutputFolder"))
    Protected doneFolder As String = ProjectFolder(AppSettings("DoneFolder"))
    Protected errorFolder As String = ProjectFolder(AppSettings("ErrorFolder"))
    Protected logFolder As String = ProjectFolder(AppSettings("LogFolder"))

    Protected header As String
    Protected template As String

    Public Sub Initiate()

        'Start
        Directory.CreateDirectory(logFolder)
        WriteLogNoDate(logFolder, "---------------------------------------------------------------------------------------------------------")
        WriteLog(logFolder, "Sports results conversion starting...")

        'Load content files, (styles)
        ' We remove the old ContentTemplate.
        'Try
        'Dim reader As New StreamReader(ProjectFolder(AppSettings("ContentTemplate")), Encoding.GetEncoding("iso-8859-1"))
        'template = reader.ReadToEnd()
        'reader.Close()
        'WriteLog(logFolder, "Content template " & ProjectFolder(AppSettings("ContentTemplate")) & " loaded.")
        'Catch ex As Exception
        'Error
        'WriteErr(logFolder, "Error loading content template (" & ProjectFolder(AppSettings("ContentTemplate")) & ").", ex)
        'End Try

        'Load FipHeader
        Try
            Dim reader As StreamReader = New StreamReader(ProjectFolder(AppSettings("FipHeader")), Encoding.GetEncoding("iso-8859-1"))
            header = reader.ReadToEnd()
            reader.Close()
            WriteLog(logFolder, "FIP header file " & ProjectFolder(AppSettings("FipHeader")) & " loaded.")
        Catch ex As Exception
            'Error
            WriteErr(logFolder, "Error loading FIP header (" & ProjectFolder(AppSettings("FipHeader")) & ").", ex)
        End Try


        'Conversion objects
        FISconvert = New FIS_Live
        IBUconvert = New IBU_Results
        SCGconvert = New SCG_Results
        NMconvert = New FIS_NM

        'Load nation map
        Dim nationmapfile As String = ProjectFolder(AppSettings("NationMapFile"))
        Dim map As New XmlDocument

        Try
            map.Load(nationmapfile)

            Dim nodes As XmlNodeList
            Dim node As XmlNode
            nodes = map.SelectNodes("/nations/nation")

            'Fill hash
            For Each node In nodes
                nation_map(node.Attributes("code").Value) = node.Attributes("name").Value
            Next

            WriteLog(logFolder, "Nation map loaded: " & nation_map.Count & " nations.")
        Catch ex As Exception
            'Error loading nation map
            WriteErr(logFolder, "Error nation map (" & nationmapfile & ") file.", ex)
        End Try


        'Load folder settings
        Dim settingsfile As String = ProjectFolder(AppSettings("FolderJobFile"))
        Try
            map.Load(settingsfile)

            Dim nodes As XmlNodeList
            Dim node As XmlNode
            nodes = map.SelectNodes("/folderJobs/job")

            'Fill hash
            For Each node In nodes
                Dim job As New FolderJob

                job.folder = node.Attributes("folder").Value
                Directory.CreateDirectory(job.folder)

                Select Case node.Attributes("type").Value
                    Case "langrenn"
                        job.type = EventType.CrossCountry
                    Case "skiskyting"
                        job.type = EventType.Biathlon
                    Case "skihopp"
                        job.type = EventType.SkiJump
                    Case "alpint"
                        job.type = EventType.Alpint
                    Case "kombinert"
                        job.type = EventType.Kombinert
                    Case "speedskating"
                        job.type = EventType.SpeedSkating
                    Case "FIS"
                        job.type = EventType.FIS
                    Case "NM"
                        job.type = EventType.NM

                End Select

                Select Case node.Attributes("format").Value
                    Case "fellesstart"
                        job.format = FormatType.CommonStart
                    Case "enkeltstart"
                        job.format = FormatType.IndividualStart
                    Case "stafett"
                        job.format = FormatType.Relay
                    Case "enomgang"
                        job.format = FormatType.OneRound
                    Case "toomganger"
                        job.format = FormatType.TwoRounds
                    Case "kombinertnormal"
                        job.format = FormatType.KombineretNormal
                    Case "speedskatingnormal"
                        job.format = FormatType.SpeedSkatingNormal
                    Case "FIS"
                        job.format = FormatType.FIS

                End Select

                JobList.Add(job)
            Next

            WriteLog(logFolder, "Job folders loaded: " & JobList.Count & " jobs.")
        Catch ex As Exception
            'Error folder job settings
            WriteErr(logFolder, "Error loading folder job list.", ex)
        End Try

        'Other settings
        Try
            notabeneError = AppSettings("GenerateNotabeneError")
            timerInterval = AppSettings("pollTimerInterval") * 1000
            fileWait = AppSettings("fileWait") * 1000

            Directory.CreateDirectory(notabeneOutput)
            Directory.CreateDirectory(doneFolder)
            Directory.CreateDirectory(errorFolder)
        Catch ex As Exception
            WriteErr(logFolder, "Error loading settings.", ex)
        End Try

        'Ready to go
        PollTimer.Start()
        WriteLog(logFolder, "Sports results conversion started.")
    End Sub

    Sub Quit()
        PollTimer.Stop()
        WriteLogNoDate(logFolder, "---------------------------------------------------------------------------------------------------------")
        WriteLog(logFolder, "Sports results conversion stopped.")
    End Sub

    Public Shared Function ProjectFolder(ByVal str As String) As String
        Return Path.Combine(AppSettings("RootFolder"), str)
    End Function


    Private Sub PollTimer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles PollTimer.Elapsed
        Dim Debug As Boolean = MainConvertModule.ProjectFolder(AppSettings("Debug"))
        'Timer kicked, loop folders
        PollTimer.Stop()

        Dim j As FolderJob
        Dim f As String
        Dim files As String()

        Dim outfile As String
        Dim output As String
        Dim body As String
        Dim count As Integer

        'Loop jobs
        For Each j In JobList

            'Get new files
            Try
                files = Directory.GetFiles(j.folder)
                System.Threading.Thread.Sleep(fileWait)
            Catch ex As Exception
                WriteErr(logFolder, "Error gettings files from " & j.folder, ex)
            End Try

            'Loop files
            For Each f In files

                'Logging
                Try
                    WriteLogNoDate(logFolder, "---------------------------------------------------------------------------------------------------------")
                    WriteLog(logFolder, "New file: " & f)
                Catch ex As Exception
                    WriteErr(logFolder, "Logging error.", ex)
                End Try

                'Fill header
                output = header
                ' outfile = notabeneOutput & "\" & "WEB" & File.GetCreationTime(f).ToString("yyyyMMdd-HHmmss") & "-" & j.type.ToString & ".htm"
                outfile = notabeneOutput & "\" & "WEB" & File.GetCreationTime(f).ToString("yyyyMMdd-HHmmss") & "-" & j.type.ToString & ".xml"
                output = output.Replace("<!--id-->", "WEB" & File.GetCreationTime(f).ToString("yyyyMMdd-HHmmss"))

                'Try to convert
                Try
                    Select Case j.type
                        Case EventType.CrossCountry
                            output = output.Replace("<!--signatur-->", "fis")
                            output = output.Replace("<!--stikkord-->", "langrenn-vc-res")
                            output = output.Replace("<!--kategori-->", "Langrenn;")
                            output = output.Replace("<!--tittel-->", "Langrenn:")

                            count = FISconvert.CC_General(f, j.format, body)
                        Case EventType.SkiJump
                            output = output.Replace("<!--signatur-->", "fis")
                            output = output.Replace("<!--stikkord-->", "hopp-vc-res")
                            output = output.Replace("<!--kategori-->", "Hopp;")
                            output = output.Replace("<!--tittel-->", "Hopp:")

                            count = FISconvert.SJ_General(f, j.format, body)

                        Case EventType.Biathlon
                            output = output.Replace("<!--signatur-->", "ibu")
                            output = output.Replace("<!--stikkord-->", "skiskyting-vc-res")
                            output = output.Replace("<!--kategori-->", "Skiskyting;")
                            output = output.Replace("<!--tittel-->", "Skiskyting:")

                            count = IBUconvert.BI_General(f, j.format, body)

                        Case EventType.Alpint
                            output = output.Replace("<!--signatur-->", "fis")
                            output = output.Replace("<!--stikkord-->", "alpint-vc-res")
                            output = output.Replace("<!--kategori-->", "Alpint;")
                            output = output.Replace("<!--tittel-->", "Alpint:")

                            count = FISconvert.AL_General(f, j.format, body)

                        Case EventType.Kombinert
                            output = output.Replace("<!--signatur-->", "fis")
                            output = output.Replace("<!--stikkord-->", "kombinert-vc-res")
                            output = output.Replace("<!--kategori-->", "Kombinert;")
                            output = output.Replace("<!--tittel-->", "Kombinert:")

                            count = FISconvert.CC_General(f, j.format, body)

                        Case EventType.SpeedSkating
                            output = output.Replace("<!--signatur-->", "scg")
                            output = output.Replace("<!--stikkord-->", "skoyter-vc-res")
                            output = output.Replace("<!--kategori-->", "Sk�yter;")
                            output = output.Replace("<!--tittel-->", "Sk�yter:")
                            If Debug = True Then WriteLog(logFolder, "G�r til SCGconvert.SCG_General")
                            count = SCGconvert.SCG_General(f, j.format, body)

                        Case EventType.FIS
                            output = output.Replace("<!--signatur-->", "fis")
                            output = output.Replace("<!--stikkord-->", "fis-vc-cupstilling")
                            output = output.Replace("<!--kategori-->", "FIS;")
                            output = output.Replace("<!--tittel-->", "FIS:")
                            If Debug = True Then WriteLog(logFolder, "G�r til FISconvert.FIS_General")
                            count = FISconvert.FIS_General(f, j.format, body)

                        Case EventType.NM
                            output = output.Replace("<!--signatur-->", "fis")
                            output = output.Replace("<!--stikkord-->", "langrenn-nm-res")
                            output = output.Replace("<!--kategori-->", "Langrenn;")
                            output = output.Replace("<!--tittel-->", "Ski-NM:")
                            If Debug = True Then WriteLog(logFolder, "G�r til NMconvert.CC_General")
                            count = NMconvert.CC_General(f, j.format, body)

                    End Select

                    If count = 0 Then
                        Dim ex As New Exception("Parse error (" & j.type.ToString & "): No items found in file '" & f & "'")
                        body = ""
                        Throw ex
                    Else
                        WriteLog(logFolder, j.type.ToString & " : " & count & " items found.")
                    End If

                Catch ex As Exception
                    'Conversion error
                    WriteLog(logFolder, ex.message)
                    WriteErr(logFolder, "Parsing failed.", ex)
                End Try

                'Simple content check
                If body <> "" Then
                    ' body = "<p class=""Infolinje"">Infolinje</p>" & vbCrLf & "<p class=""Ingress"">Ingress</p>" & vbCrLf & body
                    body = "<p lede=""true"" class=""lead"">Ingress</p>" & vbCrLf & body
                    ' output &= template.Replace("<!--body-->", body)
                    output = output.Replace("<!--body-->", body)

                    Try
                        Dim writer As New StreamWriter(outfile, False, Encoding.GetEncoding("iso-8859-1"))
                        writer.WriteLine(output)
                        writer.Close()
                        WriteLog(logFolder, "Data written to " & outfile)

                        File.Copy(f, doneFolder & "\" & Path.GetFileNameWithoutExtension(f) & "-" & File.GetCreationTime(f).ToString("yyyyMMdd-HHmmss") & Path.GetExtension(f), True)
                        File.Delete(f)
                        WriteLog(logFolder, f & " moved to donefolder.")
                    Catch ex As Exception
                        Try
                            'Outputfile error
                            WriteErr(logFolder, "File output error.", ex)

                            File.Copy(f, errorFolder & "\" & Path.GetFileNameWithoutExtension(f) & "-" & File.GetCreationTime(f).ToString("yyyyMMdd-HHmmss") & Path.GetExtension(f), True)
                            File.Delete(f)
                        Catch
                        End Try
                    End Try

                Else
                    'Other error, hopefully logged earlier
                    If notabeneError Then
                        Try
                            ' body = "<p class=""Infolinje"">Konvertering feilet!</p>" & vbCrLf & "<p class=""Ingress"">Fant ingen ut�verdata for " & j.type.ToString & ".</p>" & vbCrLf
                            body = "<p lede=""true"" class=""lead"">Konvertering feilet! Fant ingen ut�verdata for " & j.type.ToString & ".</p>" & vbCrLf

                            ' output &= template.Replace("<!--body-->", body)
                            output &= header.Replace("<!--body-->", body)

                            Dim writer As New StreamWriter(outfile, False, Encoding.GetEncoding("iso-8859-1"))
                            writer.WriteLine(output)
                            writer.Close()
                        Catch
                        End Try
                    End If

                    Try
                        WriteLog(logFolder, "Data missing.")
                        File.Copy(f, errorFolder & "\" & Path.GetFileNameWithoutExtension(f) & "-" & File.GetCreationTime(f).ToString("yyyyMMdd-HHmmss") & Path.GetExtension(f), True)
                        File.Delete(f)
                    Catch
                    End Try
                End If
            Next

        Next

        'Restart
        PollTimer.Interval = timerInterval
        PollTimer.Start()
    End Sub

End Class
