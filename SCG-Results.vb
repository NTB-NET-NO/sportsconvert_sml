Imports System.IO
Imports System.xml
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Configuration.ConfigurationSettings
Imports ntb_FuncLib.LogFile

Public Class SCG_Results

    Protected Class athlete
        'General
        Public rank As String
        Public bib As String
        Public name As String
        Public nat_code As String
        Public nation As String
        'Speedskating
        Public time As String
        Public diff As String

        'Construktor 
        'Speedskating
        Public Sub New(ByVal rank_in As String, ByVal bib_in As String, ByVal name_in As String, ByVal natcode_in As String, ByVal time_in As String, ByVal diff_in As String)
            Dim logFolder As String = MainConvertModule.ProjectFolder(AppSettings("LogFolder"))
            Dim Debug As Boolean = MainConvertModule.ProjectFolder(AppSettings("Debug"))
            If Debug = True Then WriteLog(logFolder, "Er i SCGconvert.sub New")
            rank = rank_in
            If Debug = True Then WriteLog(logFolder, "Vi har: Rank" & rank)
            bib = bib_in
            If Debug = True Then WriteLog(logFolder, "Vi har bib: " & bib)
            name = name_in
            If Debug = True Then WriteLog(logFolder, "Vi har name: " & name)
            nat_code = natcode_in
            If Debug = True Then WriteLog(logFolder, "Vi har nat_code: " & nat_code)
            diff = diff_in
            If Debug = True Then WriteLog(logFolder, "Vi har diff: " & diff)
            If Debug = True Then WriteLog(logFolder, "Vi har: " & rank & " " & bib & " " & name & " " & nat_code & " " & diff)

            nation = MainConvertModule.nation_map(nat_code)
            If nation = "" Then nation = natcode_in
            If Debug = True Then WriteLog(logFolder, "Vi har nation: " & nation)
            time = time_in

            Dim lastNameFirst As Boolean
            lastNameFirst = False
            If nation Like "Kina" Or nation Like "S�r-Korea" Or nation Like "Nord-Korea" Then
                If Debug = True Then WriteLog(logFolder, "Vi har nation: " & nation & " og setter lastnamefirst til true")
                lastNameFirst = True
            Else
                If Debug = True Then WriteLog(logFolder, "Vi har nation: " & nation & " og setter lastnamefirst til false")
                lastNameFirst = False
            End If
            If Debug = True Then WriteLog(logFolder, "Vi snur navn")
            name = ConvertTools.RotateAndFixName(name, , lastNameFirst)
            If Debug = True Then WriteLog(logFolder, "Vi har name: " & name)


            If Debug = True Then WriteLog(logFolder, "Character map substitutions")
            'Character map substitutions
            Dim subst As String() = character_map(nat_code)
            If Debug = True Then WriteLog(logFolder, "subst")
            If Not subst Is Nothing Then
                Dim i As Integer
                For i = 0 To subst.GetLength(0) - 1
                    name = name.Replace(subst(i), subst(i + 1))
                    i += 1
                Next
                If Debug = True Then WriteLog(logFolder, "Ferdig med for loop")
            End If

        End Sub

    End Class

    Protected athlete_list As ArrayList
    Protected Shared character_map As New Hashtable

    'Object constructor creates object and loads mapping lists
    Public Sub New()

        athlete_list = New ArrayList

        'Load character map
        Dim charmapfile As String = MainConvertModule.ProjectFolder(AppSettings("CharacterMapFile"))
        Dim map As New XmlDocument

        Try
            map.Load(charmapfile)

            Dim nodes As XmlNodeList
            Dim nd1, nd2 As XmlNode
            nodes = map.SelectNodes("/character-maps/nation")

            'Fill hash
            For Each nd1 In nodes
                If nd1.ChildNodes().Count > 0 Then

                    Dim c As Integer = 0
                    Dim charset((nd1.ChildNodes().Count * 2) - 1) As String
                    For Each nd2 In nd1.ChildNodes()
                        charset(c) = nd2.Attributes("value").Value
                        charset(c + 1) = nd2.Attributes("replace").Value
                        c += 2
                    Next

                    character_map(nd1.Attributes("code").Value) = charset
                End If
            Next

            WriteLog(New String(MainConvertModule.ProjectFolder(AppSettings("LogFolder"))), "SCG - Character converison map loaded (" & charmapfile & "): " & CStr(character_map.Count) & " conversion sets.")
        Catch ex As Exception
            'Error loading nation map
            WriteErr(New String(MainConvertModule.ProjectFolder(AppSettings("LogFolder"))), "Error loading charachter conversion map (" & charmapfile & ") for IBU", ex)
        End Try

    End Sub

    'Takes a speedskating HTML list and fills the athlete list
    Protected Function ParseDataSCG(ByVal filename As String) As Integer
        Dim logFolder As String = MainConvertModule.ProjectFolder(AppSettings("LogFolder"))
        Dim Debug As Boolean = MainConvertModule.ProjectFolder(AppSettings("Debug"))
        If debug = True Then WriteLog(logFolder, "Er i SCGconvert.ParseDataSCG")

        Dim input As StreamReader = New StreamReader(filename, Encoding.GetEncoding("iso-8859-1"))
        Dim content As String = input.ReadToEnd()
        input.Close()

        athlete_list.Clear()

        ' Remove span
        ' 
        If Debug = True Then WriteLog(logFolder, "Remove tags")
        Dim span As Regex = New Regex("<span\b[^>]*>(\d)+(.*?)</span>", RegexOptions.Compiled) 'Drop all tags
        content = span.Replace(content, " ")
        WriteLog(logFolder, content)

        'Remove tags
        If Debug = True Then WriteLog(logFolder, "Remove tags")
        Dim tags As Regex = New Regex("<[^>]+>", RegexOptions.Compiled) 'Drop all tags
        content = tags.Replace(content, " ")
        WriteLog(logFolder, content)

        'Split off header
        If Debug = True Then WriteLog(logFolder, "Split off header")
        Dim topsplit As String = "Rankings"
        content = content.Substring(content.IndexOf(topsplit) + topsplit.Length)

        'Find results
        If Debug = True Then WriteLog(logFolder, "Find results with RegExp")
        ' Dim res As Regex = New Regex("(\d+)\s+(\d+)\s+([\w\- ]+?)\s+(\w{3})\s+([\d:\.]+)(\(\d\))?\s+([\d:\.]+)?", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        ' Dim res As Regex = New Regex("(\d+)\s+(\d+)\s+([\w\- ]+?)\s+(\w{3})\s+([\d:\.]+)(\(\d\))?\s+([\d:\.]+)?", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        Dim res As Regex = New Regex("([0-9]+)\s+([0-9]+)\s+([a-z-]+)\s+([a-z]+)\s+([a-z]{3})\s+([0-9]+:[0-9]+\.[0-9]+|[0-9]+\.[0-9]+)", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        ' (\d+)\s+(\d+)\s+([\w\- ]+?)\s+([\w\- ]+?)\s+(\w{3})\s+([\d:\.]+)\s+([\d:\.]+)\s+(\d+)\s+", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        ' (\d+)\s+(\d+)\s+(\w+)\s+(\w+)\s+(\w{3})\s+([\d:\.]+)\s
        ' (\d+)\s+(\d+)\s+(\w+)\s+(\w+|[a-z]+-[a-z]+)\s+(\w{3})\s+(\d{2}\.)(\d{2})\s+
        ' \s(\w{3})\s+((\d{2}\.)(\d{2}))\s+
        ' \s(\w{3})\s+((\d{2}\.\d{2}))\s+
        Dim first As TimeSpan
        Dim m As Match = res.Match(content)
        While m.Success

            Dim rank As String = m.Groups(1).Value
            Dim bib As String = m.Groups(2).Value
            Dim name As String = m.Groups(3).Value & " " & m.Groups(4).Value ' Lagt til groups 4
            Dim nation As String = m.Groups(5).Value ' endret fra 4 til 5


            If Debug = True Then WriteLog(logFolder, "Data vi har: " & rank & " " & bib & " " & name & " " & nation)

            'Fetch the time data
            ' this part here is no longer needed as ISU provides diffs and more in their results
            'Dim time, diff As TimeSpan
            'If m.Groups(7).Value = "" Then
            '    ' first = GetTimeSpan(m.Groups(5).Value, 10) ' Endret fra 10 til 5
            '    first = GetTimeSpan(m.Groups(6).Value, 10) ' Endret fra 5 til 6
            '    time = first
            'Else
            '    diff = GetTimeSpan(m.Groups(7).Value, 10) ' Endret fra 10 til 5
            '    time = first.Add(diff)
            'End If

            'Format times
            Dim strTime, strDiff As String
            'strTime = ConvertTools.FormatTimeSkate(time, 10) ' Endret fra 10 til 5
            'strDiff = ConvertTools.FormatTimeSkate(diff, 10) ' Endret fra 10 til 5
            strTime = m.Groups(6).Value
            strDiff = m.Groups(7).Value

            strTime = strTime.Replace(".", ",").ToString()
            strTime = strTime.Replace(":", ".").ToString()


            If Debug = True Then WriteLog(logFolder, "Mer data: " & strTime & " " & strDiff)

            If Debug = True Then WriteLog(logFolder, "Lager ny athlete")
            Dim athlete As New athlete(rank, bib, name, nation, strTime, strDiff)
            If Debug = True Then WriteLog(logFolder, "Ferdig med � lage ny athlete")

            If Debug = True Then WriteLog(logFolder, "Legger til i athlete_list")

            athlete_list.Add(athlete)
            If Debug = True Then WriteLog(logFolder, "Lagt til i athlete_list")
            m = m.NextMatch()
        End While
        If Debug = True Then WriteLog(logFolder, "Antall ut�vere: " & athlete_list.Count)
        Return athlete_list.Count
    End Function

    'Returns a formated resultlist, for individual starts
    Public Function SCG_General(ByVal filename As String, ByVal format As MainConvertModule.FormatType, ByRef body As String) As String
        Dim logFolder As String = MainConvertModule.ProjectFolder(AppSettings("LogFolder"))
        Dim Debug As Boolean = MainConvertModule.ProjectFolder(AppSettings("Debug"))
        If Debug = True Then WriteLog(logFolder, "Er i SCGconvert.SCG_General")
        'Fill the athlete list
        Dim c As Integer = ParseDataSCG(filename)
        Dim ret As String = ""

        Dim i As Integer = 1
        Dim at As athlete
        For Each at In athlete_list

            'Add paragraphs
            If i = 1 Then
                ' ret &= "<p class=""Brdtekst"">"
                ret &= "<p class=""txt"">"
            ElseIf i Mod 10 = 1 Then
                ' ret &= "</p>" & vbCrLf & "<p class=""BrdtekstInnrykk"">"
                ret &= "</p>" & vbCrLf & "<p class=""txt-ind"">"
            End If

            'Fill in results
            Select Case format
                Case MainConvertModule.FormatType.IndividualStart
                    ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                Case Else
                    If i = 1 Then
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                    ElseIf i = 2 Then
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                    Else
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                    End If
                    '    ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & " (" & at.time & "), "
                    'Case Else
                    '    If i = 1 Then
                    '        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & " (" & at.time & "), "
                    '    ElseIf i = 2 Then
                    '        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & " min bak (" & at.time & "), "
                    '    Else
                    '        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & " (" & at.time & "), "
                    '    End If
            End Select

            'Update counter
            i += 1
        Next

        'Finish off and return
        ret &= "</p>"
        body = ret

        Return c
    End Function


End Class
